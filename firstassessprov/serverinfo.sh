#!/bin/bash
# description: Set my web page
# chkconfig: 2345 99 99
case $1 in
    'start')
            echo -e "<html>\nHello there<br>My IP address is $(ifconfig | grep 192 | awk '{print $2}'| sed 's/^.*://')\n</html>" >/var/www/html/index.html
            ;;
    *)
        echo "Not a valid argument"
        ;;
esac 
