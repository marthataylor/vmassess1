
# Will install, enable, and start apache/httpd upon vagrant up
apt -y update

apt -y install apache2

# Copying the index file to the VM
cp /vagrant/firstassessprov/indexubuntu.html /var/www/html/index.html

# Copying the new init file to
cp /vagrant/firstassessprov/serverinfo.sh /etc/init.d
