
# Will install, enable, and start apache/httpd upon vagrant up
yum -y install httpd
systemctl enable httpd
systemctl start httpd


# Copying the index file to the VM
cp /vagrant/firstassessprov/indexrhel.html /var/www/html/index.html

cp /vagrant/firstassessprov/serverinfo.sh /etc/init.d
